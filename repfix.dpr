library repfix;

uses
  Windows,
  SysUtils,
  URepFix in 'URepFix.pas';

{$E bwl}
{$R *.res}

const
  BWL_API         = 4;
  STARCRAFT_BUILD = 13;	     // 1.16.1

  PLUGIN_NAME       = 'RepFix';
  PLUGIN_DESRIPTION = 'Fixes crash when watching a replay caused by chat text while game paused.'#13#10#13#10'Author: Python_Max';
  PLUGIN_URL        = 'http://www.iccup.com/';

type
  TExchangeData = packed record
    iPluginAPI: Integer;
    iStarCraftBuild: Integer;
    bConfigDialog: Boolean;
    bNotSCBWmodule: Boolean;
  end;


// BWLauncher API implementation

procedure GetPluginAPI(var AData: TExchangeData); cdecl;
begin
  // BWL Gets version from Resource - VersionInfo
  with AData do begin
    iPluginAPI      := BWL_API;
    iStarCraftBuild := STARCRAFT_BUILD;
    bConfigDialog   := False; // not implemented
    bNotSCBWmodule  := False; // injection required
  end;
end;

procedure GetData(szName, szDescription, szUpdateUrl: PChar); cdecl;
begin
  // if necessary you can add Initialize function here

  StrPCopy(szName, PLUGIN_NAME);
  StrPCopy(szDescription, PLUGIN_DESRIPTION);
  StrPCopy(szUpdateUrl, PLUGIN_URL);
end;

//
//Functions called by BWLauncher
//

function OpenConfig: Boolean; cdecl;
begin
  // show dialog
  Result := True; 
end;

function ApplyPatchSuspended(hProcess: THandle;
  dwProcessId: DWORD): Boolean; cdecl;
begin
  // This function is called on suspended process
  // Durning the suspended process some modules of starcraft.exe may not yet exist.
  // the dwProcessID is not checked, its the created pi.dwProcessId

  Result := True;
end;

function GetModuleName(hModule: THandle): String;
var
  Buf: Array[0..MAX_PATH] of Char;
begin
  ZeroMemory(@Buf[0], MAX_PATH);
  GetModuleFileName(hModule, @Buf[0], MAX_PATH);
  Result := String(PChar(@Buf[0]));
end;

procedure Inject(hProcess: THandle);
var
  hThread: THandle;
  pRemoteDllName, ThreadProc: Pointer;
  ThreadId, dwWritten: DWORD;
  ExitCode:Cardinal;
  ModuleName: String;
begin
  ModuleName := GetModuleName(HInstance);
  pRemoteDllName := VirtualAllocEx(hProcess, nil, Length(ModuleName),
    MEM_COMMIT or MEM_TOP_DOWN, PAGE_READWRITE);
  if (pRemoteDllName = nil) then begin
    raise Exception.Create('VirtualAllocEx failed');
  end;

  if (not WriteProcessMemory(hProcess, pRemoteDllName, PChar(ModuleName),
      Length(ModuleName), dwWritten)) then begin
    VirtualFreeEx(hProcess, pRemoteDllName, 0, MEM_RELEASE);
    raise Exception.Create('WriteProcessMemory failed');
  end;

  ThreadProc := GetProcAddress(GetModuleHandle('kernel32.dll'), 'LoadLibraryA');
  if (ThreadProc = nil) then begin
    raise Exception.Create('ThreadProc (LoadLibraryA) not found');
  end;

  hThread := CreateRemoteThread(hProcess, nil, 0, ThreadProc,
    pRemoteDllName, 0, ThreadId);
  if (hThread = 0) then begin
    raise Exception.Create('CreateRemoteThread failed');
  end;

  ExitCode := 0;
  WaitForSingleObject(hThread, INFINITE);
  GetExitCodeThread(hThread, ExitCode);
  CloseHandle(hThread);
  VirtualFreeEx(hProcess, pRemoteDllName, 0, MEM_RELEASE);

  if (ExitCode = 0) then begin
    raise Exception.CreateFmt('Remote thread (LoadLibraryA) failed (exit code: %d)', [ExitCode]);
  end;
end;

function ApplyPatch(hProcess: THandle;
  dwProcessId: DWORD): Boolean; cdecl;
begin
	// This fuction is called after
	// ResumeThread(pi.hThread);
	// WaitForInputIdle(pi.hProcess, INFINITE);
	// EnableDebugPriv()

  Inject(hProcess);

  Result := True;
end;

exports
  GetPluginAPI,
  GetData,
  OpenConfig,
  ApplyPatchSuspended,
  ApplyPatch;

procedure DllMain(dwReason: DWORD);
begin
  case dwReason of
    DLL_PROCESS_DETACH: begin
      Stop;
    end;
  end;
end;

begin
  if (StrIComp(PChar(ExtractFileName(ParamStr(0))), 'starcraft.exe') = 0) then begin
    DLLProc := @DllMain;
    Start;
  end;
end.
