Replay fix for StarCraft: Brood War 1.16.1

Fixes crash when watching a replay caused by chat text while game paused.