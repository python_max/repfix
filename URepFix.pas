unit URepFix;

interface

uses
  Windows, SysUtils, Classes;

  procedure Start;
  procedure Stop;

implementation

var
  Actions: TMemoryStream = nil;
  Terminated: Boolean = False;

const
  COPY_ACTION   = $004CE0A5;
(*
004CE0A5  E8 C220F4FF    CALL <JMP.&storm.#491>            ;  // SMemCopy
*)
  GET_ACTION    = $004871EF;
(*
004871EF  8D87 B0526500  LEA EAX,DWORD PTR DS:[EDI+6552B0]
*)
  STATIC_BUFFER = $006552B0;

// hooked functions start
procedure CopyAction(Dest, Src: Pointer; Count: Cardinal); stdcall;
begin
  if (Actions = nil) then begin
    Actions := TMemoryStream.Create();
    if (Cardinal(Dest) <> STATIC_BUFFER) then begin
      Actions.Write(Pointer(STATIC_BUFFER)^, Cardinal(Dest) - STATIC_BUFFER);
    end;
  end;
  if (Cardinal(Dest) = STATIC_BUFFER) then begin
    Actions.Position := 0;
    Actions.Size := 0;
  end;
  Actions.Size := Actions.Size + Count;
  Actions.Write(Src^, Count);
end;

function GetAction(Index: Cardinal): Pointer; stdcall;
begin
  if (Actions = nil) then begin
    Result := Pointer(STATIC_BUFFER + Index);
  end else begin
    Result := Pointer(Cardinal(Actions.Memory) + Index);
  end;
end;
// hooked functions end

function Read(AOffset: Cardinal; ABuffer: Pointer; ACount: Cardinal): Boolean;
var
  dwRead: Cardinal;
begin
  Result := ReadProcessMemory(GetCurrentProcess(), Pointer(AOffset), ABuffer, ACount, dwRead);
  Result := Result and (dwRead = ACount);
end;

function Write(AOffset: Cardinal; ABuffer: Pointer; ACount: Cardinal): Boolean;
var
  dwWritten: Cardinal;
begin
  Result := WriteProcessMemory(GetCurrentProcess(), Pointer(AOffset), ABuffer, ACount, dwWritten);
  Result := Result and (dwWritten = ACount); 
end;

procedure Patch;
var
  data: Cardinal;
begin
  data := Cardinal(@CopyAction) - COPY_ACTION - 5; // relative call argument
  Write(COPY_ACTION + 1, @data, 4);

  Write(GET_ACTION, PChar(#$57#$E8), 2); // push edi, call [arg]
  data := Cardinal(@GetAction) - (GET_ACTION + 1) - 5; // relative call argument
  Write(GET_ACTION + 2, @data, 4);
end;

procedure Unpatch;
var
  data: Cardinal;
begin
  FreeAndNil(Actions);
  
  data := $FFF420C2;
  Write(COPY_ACTION + 1, @data, 4);

  Write(GET_ACTION, PChar(#$8D#$87), 2);
  data := STATIC_BUFFER;
  Write(GET_ACTION + 2, @data, 4);
end;

procedure Timer;
const
  IN_GAME   = $006D11EC;
//  IN_GAME   = $1505BBC8;
  IS_REPLAY = $006D0F14;
var
  GameState: Cardinal;
  IsReplayInProgress: Boolean;
begin
  IsReplayInProgress := False;
  while (not Terminated) do begin
    Read(IN_GAME, @GameState, 4);
    if (GameState <> 0) then begin
      Read(IS_REPLAY, @GameState, 4);
      if ((GameState <> 0) and (not IsReplayInProgress)) then begin
        IsReplayInProgress := True;
        Patch;
      end;
    end else if (IsReplayInProgress) then begin
      IsReplayInProgress := False;
      Unpatch;
    end;
    Sleep(500);
  end;
end;

procedure Start;
var
  ThreadId: Cardinal;
  hThread: Thandle;
begin
  hThread := CreateThread(nil, 0, @Timer, nil, CREATE_SUSPENDED, ThreadId);
  SetThreadPriority(hThread, THREAD_PRIORITY_BELOW_NORMAL);
  ResumeThread(hThread);
end;

procedure Stop;
begin
  Terminated := True;
end;

end.
